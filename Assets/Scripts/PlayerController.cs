using System.Collections;
using System.Collections.Generic;
// using DG.Tweening;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Rigidbody2D rb;

    public float _speed = 10f;

    public float _jumpForce = 50f;
    public float _dashSpeed = 20f;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        // walk
        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");
        float xRaw = Input.GetAxisRaw("Horizontal");
        float yRaw = Input.GetAxisRaw("Vertical");
        Vector2 dir = new Vector2(x, y);

        Walk(dir);
        // Jump
        if (Input.GetButtonDown("Jump"))
        {
            Jump(Vector2.up);
        }

        if (Input.GetButtonDown("Fire1"))
        {
            if (xRaw != 0 || yRaw != 0)
                Dash(xRaw, yRaw);
        }
    }

    public void Walk(Vector2 dir)
    {
        rb.velocity = new Vector2(dir.x * _speed, rb.velocity.y);
    }

    private void Jump(Vector2 dir)

    {
        rb.velocity = new Vector2(rb.velocity.x, 0);
        rb.velocity += dir * _jumpForce;
    }

    private void Dash(float x, float y)
    {
        // Camera.main.transform.DOComplete();
        // Camera.main.transform.DOShakePosition(.2f, .5f, 14, 90, false, true);
        rb.velocity = Vector2.zero;
        Vector2 dir = new Vector2(x, 0);

        rb.velocity += dir.normalized * _dashSpeed;
    }


}




